all: docker_down docker_up test deploy
docker_down:
	docker-compose down

docker_up:
	docker-compose up -d 

test:
	curl http://localhost:80
	curl http://localhost:80/public
	curl http://localhost:80/private

deploy:
	cd kubernetes && kubectl apply -f k8s.yaml